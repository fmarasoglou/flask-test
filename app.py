from flask import Flask
import logging


app = Flask(__name__)

@app.route("/")
def hello_world():
    return "<p>Hello, World!</p>"


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    logging.info('application started')

    app.run(host="0.0.0.0", port=5000, debug=True)